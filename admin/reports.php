<?php
require_once '../functions.php';
require_once 'logincheck.php';
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>
<div class="container-fluid">
    <div id="superdashboard">
        <div class="row">
            

          <div class="col-12 col-md-4"> 
          <h6>Videos</h6>
                <?php
                $exhib = new Exhibitor();
                $vidList = $exhib->getVideoViewersCount();
                //var_dump($vidList);
                if (!empty($vidList)) {
                ?>
                    <table class="table table-borderless table-striped">
                        <?php
                        foreach ($vidList as $vid) {
                        ?>
                            <tr>
                                <td><?= '<b>' . $vid['video_title'] ?></td>
                                <td><?= $vid['cnt']; ?></td>
                                <td width="50"><a href="videoanalytics.php?e=<?php echo $vid['video_id']; ?>"><i class="fas fa-download"></i></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>

                <?php
                }

                ?>
            </div> 

            

        </div>
    </div>
</div>
<?php
require_once 'scripts.php';
?>
<?php
require_once 'footer.php';
?>