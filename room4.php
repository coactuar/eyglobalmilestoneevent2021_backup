<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = '513663703cddb12a71054baaba0af1ea19bd43fd0cc732539e1629195878adf3';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'room1';
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/img/room.jpg" usemap="#image-map">
            <map name="image-map">
                <area onclick="showAttendees()" alt="Attendee List" title="Attendee List" href="#" coords="323,247,922,357,927,530,331,550" shape="poly">
                <area target="_blank" alt="Join Meeting" title="Join Meeting" href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZWFlYTQ5YTAtZWJhOC00Mjg3LThjYWYtMmI5YjU4YWM4ZDZl%40thread.v2/0?context=%7b%22Tid%22%3a%225d2d4761-845f-4e1c-a2cd-aaded29aaf65%22%2c%22Oid%22%3a%22b128961b-d272-4590-8b0e-5092df4d34fd%22%7d" coords="1161,394,1156,481,1478,471,1480,369" shape="poly">
                <area target="_blank" alt="Miro Whiteboard" title="Miro Whiteboard" href="https://miro.com/app/board/o9J_kgxbwfc=/" coords="1498,368,1498,473,1702,470,1702,351" shape="poly">
            </map>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>

<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);

    function showAttendees() {
        $.magnificPopup.open({
            items: {
                src: 'assets/resources/list4.jpg'
            },
            type: 'image'
        });
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>