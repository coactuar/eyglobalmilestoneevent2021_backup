<?php
require_once '../inc/config.php';

if (!isset($_SESSION['moderator'])) {
    header('location: ./');
}
